import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OneHotEncoder
import plotly.graph_objects as go

data = pd.read_csv('donnees.csv')
annee_debut = 2018
annee_fin = 2022
donnees_futures = data[(data['Annee_RefT'] >= annee_debut) & (data['Annee_RefT'] <= annee_fin)]

# Group the data by "Materiel" and calculate the mean CA for each category
grouped_data = donnees_futures.groupby('Materiel')['CA'].mean().reset_index()

X = grouped_data[['Materiel']]
y = grouped_data['CA']

# Perform one-hot encoding on the "Materiel" column
encoder = OneHotEncoder(sparse=False)
X_encoded = encoder.fit_transform(X)

# Preprocess data to handle missing values
imputer = SimpleImputer(strategy='mean')
X_imputed = imputer.fit_transform(X_encoded)

model = LinearRegression()
model.fit(X_imputed, y)

# Create data for predictions
materieux_predire = data['Materiel'].unique()
donnees_predire = pd.DataFrame({'Materiel': materieux_predire})

# Perform one-hot encoding on the prediction data
donnees_predire_encoded = encoder.transform(donnees_predire)

# Preprocess the prediction data using the same imputer
donnees_predire_imputed = imputer.transform(donnees_predire_encoded)

predictions = model.predict(donnees_predire_imputed)

# Create a graph to visualize the predictions
fig = go.Figure(data=[go.Bar(x=donnees_predire['Materiel'], y=predictions)])

fig.update_layout(title='Prédictions de chiffre d\'affaires pour l\'année 2023 par Materiel',
                  xaxis_title='Materiel',
                  yaxis_title='CA')

fig.show()


export_csv = donnees_predire.to_csv (r'prediction.csv', index = None, header=True)
