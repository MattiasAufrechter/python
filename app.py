import pandas as pd #pip install pandas openpyxl
import plotly_express as px #pip install plotly_express
import streamlit as st #pip install streamlit
import plotly.graph_objects as go
from sklearn.ensemble import RandomForestRegressor
import re

st.set_page_config(page_title="Ventes de Loxam", page_icon=":bar_chart:", layout="wide")


dataframe=pd.read_excel("donnees.xlsx", engine='openpyxl')

selected_cat_classes = st.sidebar.multiselect('Sélectionnez une ou plusieurs catégories de classe', ['Tout'] + list(dataframe['Cat_Classe_Designation'].unique()))
selected_agences = st.sidebar.selectbox('Sélectionnez une agence', ['Tout'] + list(dataframe['Agence_Gerante'].unique()))
selected_agences2 = st.sidebar.selectbox('Sélectionnez une ou plusieurs autres agences', ['Tout'] + list(dataframe['Agence_Gerante'].unique()))


#filtrage pour selected_agences

if selected_agences == 'Tout':
    filtered_data = dataframe  # Affiche toutes les données si rien n'est sélectionné
else:
    filtered_data = dataframe[dataframe['Agence_Gerante'] == selected_agences]


#filtrage pour selected_agences2

if selected_agences2 == 'Tout':
    filtered_data = dataframe  # Affiche toutes les données si rien n'est sélectionné
else:
    filtered_data = dataframe[dataframe['Agence_Gerante'] == selected_agences2]

st.title(":bar_chart: Ventes de Loxam")
st.markdown("## **Ventes de Loxam**")

total_ventes = int(filtered_data['CA'].sum())

st.markdown(f"**Total des ventes :** {total_ventes} €")


# Créer un graphique en colonnes pour afficher les ventes par materiel
ventes_par_materiel = filtered_data.groupby(['Cat_Classe_Designation'])['CA'].sum().reset_index()
fig_ventes_par_materiel = px.bar(ventes_par_materiel, x='Cat_Classe_Designation', y='CA', title='Ventes par matériel')

tx_rota_par_materiel = filtered_data.groupby(['Cat_Classe_Designation'])['tx_rota'].mean().reset_index()
fig_tx_rota_par_materiel = px.bar(tx_rota_par_materiel, x='Cat_Classe_Designation', y='tx_rota', title='Taux de rotation par matériel')

left_column, right_column = st.columns(2)
left_column.plotly_chart(fig_ventes_par_materiel, use_container_width=True)
right_column.plotly_chart(fig_tx_rota_par_materiel, use_container_width=True)


selected_cat_classes = [cat_class for cat_class in selected_cat_classes if cat_class != 'Tout']
available_materiels = dataframe[dataframe['Cat_Classe_Designation'].isin(selected_cat_classes)]['Materiel'].unique()
selected_materiel = st.sidebar.selectbox('Sélectionnez un matériel', ['Tout'] + list(available_materiels))
selected_data = dataframe[dataframe['Materiel'] == selected_materiel]
if selected_data.empty:
    st.write("Aucune donnée disponible pour le matériel et les années spécifiées.")
else:
    regressor = RandomForestRegressor()

    X_train = selected_data[['Annee_RefT']]
    y_train = selected_data['CA']

    regressor.fit(X_train, y_train)

    X_pred = pd.DataFrame({'Annee_RefT': [2023]})
    y_pred = regressor.predict(X_pred)

    fig = go.Figure()

    fig.add_trace(go.Scatter(
        x=selected_data['Annee_RefT'],
        y=selected_data['CA'],
        mode='markers',
        name='Données réelles'
    ))

    fig.add_trace(go.Scatter(
        x=X_pred['Annee_RefT'],
        y=y_pred,
        mode='markers',
        name='Prédictions'
    ))

    fig.update_layout(
        title=f"Chiffre d'affaires (CA) pour le matériel {selected_materiel}",
        xaxis_title="Année",
        yaxis_title="Chiffre d'affaires (CA)"
    )

    st.plotly_chart(fig, use_container_width=True)

st.write("Les Cat_Classe_Designation pour l'agence deux choisis.")

cat_classe_designation = filtered_data['Cat_Classe_Designation'].unique()

for cat_class in cat_classe_designation:
    materiels = filtered_data[filtered_data['Cat_Classe_Designation'] == cat_class]['Materiel'].unique()

    for materiel in materiels:
        selected_data = filtered_data[(filtered_data['Cat_Classe_Designation'] == cat_class) & (filtered_data['Materiel'] == materiel)]

        if selected_data.empty:
            continue

        fig1 = go.Figure()
        fig2 = go.Figure()

        fig1.add_trace(go.Scatter(
            x=selected_data['Annee_RefT'],
            y=selected_data['CA'],
            mode='lines',
            name=materiel
        ))

        fig1.update_layout(
            title=f"Chiffre d'affaires (CA) par année pour le matériel {materiel}",
            xaxis_title="Année",
            yaxis_title="Chiffre d'affaires (CA)"
        )

        
        fig2.add_trace(go.Scatter(
            x=selected_data['Annee_RefT'],
            y=selected_data['tx_rota'],
            mode='lines',
            name=materiel
        ))

        fig2.update_layout(
            title=f"Le taux de rotation par année, par materiel {materiel}",
            xaxis_title="Année",
            yaxis_title="Taux de rotation"
        )

        

        st.plotly_chart(fig1, use_container_width=True)
        st.plotly_chart(fig2, use_container_width=True)






# ---- HIDE STREAMLIT STYLE ----
hide_st_style = """
            <style>
            #MainMenu {visibility: hidden;}
            footer {visibility: hidden;}
            header {visibility: hidden;}
            </style>
            """
st.markdown(hide_st_style, unsafe_allow_html=True)